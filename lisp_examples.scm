(+ 2 "3")

(define (f x y) (+ x y))

(define (factorial x) (if (= x 1) 1 (* x (factorial (x 1)))))

(define (counter inc) (lambda (x) (set! inc (+ x inc)) inc))
(define my−count (counter 5))
(my−count 3) -> 8



(load "basic_library.scm")
